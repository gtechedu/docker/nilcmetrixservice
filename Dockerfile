FROM gtechedu/nilcmetrix

#instalando o que precisa para o servidor
RUN apt update && apt-get install -y curl apache2 php

#Limpeza de coisas desnecessárias
RUN rm /var/www/html/index.html && rm -rf /opt/text_metrics/tools/postgres && apt-get clean

#Copiando coisas que são necessárias
COPY www/. /var/www/html/
COPY gtech.py /opt/text_metrics/

#Colocando coisas no local certo
RUN mv /root/nltk_data /usr/nltk_data && chmod -R 775 /usr/nltk_data


ENV DB_DATABASE='nilcmetrix'
ENV DB_USERNAME='user'
ENV DB_PASSWORD='pass1234' 
ENV DB_HOST='db' 

EXPOSE 80


CMD apachectl -D FOREGROUND 