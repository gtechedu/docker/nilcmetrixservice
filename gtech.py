# -*- coding: utf-8 -*-
import text_metrics
import sys


nome_arquivo = sys.argv[1]  #leitura de dados como arg

try:
    with open(nome_arquivo, 'r') as arquivo:
        raw = arquivo.read()
except FileNotFoundError:
    print(f"O arquivo '{nome_arquivo}' não foi encontrado.")
except Exception as e:
    print(f"Ocorreu um erro: {e}")


raw = raw.encode("utf-8", "surrogateescape").decode("utf-8")
t = text_metrics.Text(raw)

ret = text_metrics.no_palavras_metrics.values_for_text(t).as_flat_dict()
result = '' 

for f in ret:
    m = "%s:%s," % (f, ret[f])
    result += m

print("{", result, "}")
