<?php

require 'util.php';

$ct = new Control();
$ct->processText();

class Control
{
    private $dir = '/tmp/';


    public function processText()
    {

        if (!isset($_REQUEST['text'])) {
            return serviceError('É necessário informar o texto');
        }
        
        $file = $this->dir . uniqid();
       
        file_put_contents($file, $_REQUEST['text']);
        
        $exec = 'python3 /opt/text_metrics/gtech.py ' . $file ;
        //echo $exec;
        exec($exec, $output, $returnvar);
        if ($returnvar == 0) {
            foreach ($output as $linha) {
                echo $linha;
            }
        } else {
            echo 'no return';
        }
        unlink($file);
    }


}

